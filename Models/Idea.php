<?php

namespace Models;

use function in_array;
use function is_numeric;

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 03/06/2019
 * Time: 14:38
 */
class Idea extends BaseModel
{
    protected $dateFormat = 'U';
    protected $fillable = [
        'content',
        'impact',
        'ease',
        'confidence',
    ];
    protected $hidden = [
        'updated_at',
        'user_id',
    ];

    public function getCreatedAtAttribute($value)
    {
        return (int)$value;
    }

    /**
     * Will calculate the average based on the model's ease, confidence and
     * impact
     *
     * @return float
     */
    protected
    function calculateAverage()
    {
        return ($this->confidence + $this->ease + $this->impact) / 3.0;
    }

    /**
     * Actions to be done with the model before saving to DB
     *
     * @return bool
     */
    protected
    function preSave()
    {
        parent::preSave();

        $this->average_score = $this->calculateAverage();

        return true;

    }

    /**
     * Validates if the current model can be saved to the database
     *
     * @throws \Exception
     */
    public
    function validate()
    {
        if (empty($this->content) || !strlen(trim($this->content))) {
            throw new ModelValidationException('Content is required', 400);
        }

        if (!in_array($this->impact, range(1, 10))) {
            throw new ModelValidationException('Impact should be between 1 and 10',
                400);
        }

        if (!in_array($this->ease, range(1, 10))) {
            throw new ModelValidationException('Ease should be between 1 and 10',
                400);
        }

        if (!in_array($this->confidence, range(1, 10))) {
            throw new ModelValidationException('Confidence should be between 1 and 10',
                400);
        }

        if (!User::find($this->user_id)) {
            throw new ModelValidationException('Invalid idea owner provided',
                400);
        }
    }

}