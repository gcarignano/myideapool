<?php

namespace Models;

use DateTime;
use \Exception;
use Firebase\JWT\JWT;

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 03/06/2019
 * Time: 14:38
 */
final class User extends BaseModel
{
    protected $fillable = ['name', 'email'];
    protected $hidden = [
        'password',
        'id',
        'created_at',
        'updated_at',
        'refresh_token',
    ];

    /**
     * Setter method for the password attribute.
     * The method will encrypt the password when setting it
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->checkPasswordFormat($password);

        $this->attributes['password'] = password_hash(
            $password,
            PASSWORD_BCRYPT
        );
    }

    /**
     * Validates if the current model can be saved to the database
     *
     * @throws ModelValidationException
     */
    public function validate()
    {
        if (empty($this->name) || !strlen(trim($this->name))) {
            throw new ModelValidationException('A user name is required', 400);
        }

        if ($this->isEmailInUse($this->email)) {
            throw new ModelValidationException(
                'Email already in use, please chose another one',
                400
            );
        }

        $this->checkPasswordFormat($this->password);
    }

    /**
     *
     * @param $email
     *
     * @return bool
     */
    private function isEmailInUse($email)
    {
        $this->verifyEmailFormat($email);
        $existing = User::where('email', '=', $email)->first();

        return ($existing && $existing->id != $this->id);
    }

    /**
     * Checks if the provided email is a valid email format
     *
     * @param $email
     *
     * @return true
     * @throws ModelValidationException
     */
    private function verifyEmailFormat($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ModelValidationException('Invalid email format', 400);
        }

        return true;
    }

    /**
     * Determines whether the password is secure enough
     *
     * @param string $password
     *
     * @return true
     * @throws ModelValidationException
     */
    private function checkPasswordFormat($password)
    {
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);

        if (!$uppercase) {
            throw new ModelValidationException(
                'At least an upper case is required in the password',
                400
            );
        }

        if (!$lowercase) {
            throw new ModelValidationException(
                'At least a lower case is required in the password',
                400
            );
        }

        if (!$number) {
            throw new ModelValidationException(
                'At least a number is required in the password',
                400
            );
        }

        if (strlen($password) < 8) {
            throw new ModelValidationException(
                'Password should be at least 8 characters long',
                400
            );
        }

        return true;
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email
     * address.
     *
     * @param string             $email The email address
     * @param int|string         $s     Size in pixels, defaults to 80px [ 1 -
     *                                  2048 ]
     * @param string             $d     Default imageset to use [ 404 | mp |
     *                                  identicon | monsterid | wavatar ]
     * @param string             $r     Maximum rating (inclusive) [ g | pg | r
     *                                  | x ]
     * @param bool|\Models\boole $img   True to return a complete IMG tag False
     *                                  for just the URL
     * @param array              $atts  Optional, additional key/value
     *                                  attributes to include in the IMG tag
     *
     * @return String containing either just a URL or a complete image tag
     * @source https://gravatar.com/site/implement/images/php/
     */
    function get_gravatar(
        $email,
        $s = 80,
        $d = 'mp',
        $r = 'g',
        $img = false,
        $atts = []
    ) {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }
            $url .= ' />';
        }

        return $url;
    }

    /**
     * Actions to be done with the model before saving to DB
     *
     * @return bool
     */
    protected function preSave()
    {
        parent::preSave();

        if (!$this->exists) {
            $this->avatar_url = $this->get_gravatar($this->email);
        }

        return true;

    }

    /**
     * Returns a JWT token generated from the model's data
     *
     * @return string
     */
    public function getJWT()
    {
        $now = new DateTime();
        $future = new DateTime("now +" . JWT_VALIDITY);

        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "id" => $this->id,
            "email" => $this->email,
        ];

        $token = JWT::encode($payload, JWT_SECRET, JWT_ALGORITHM);

        return $token;
    }

    /**
     * Will set the current's object refresh_token with a newly generated token
     * This method does not save the model
     *
     */
    public function refreshToken()
    {
        $this->refresh_token = base64_encode(openssl_random_pseudo_bytes(100));

        return $this;
    }

    /**
     * Returns the JWT token and refresh token for this user.
     * This refreshes the token
     *
     * @return array
     */
    public function getLoginData()
    {
        $arrRtn = [
            "jwt" => $this->getJWT(),
            "refresh_token" => $this->refresh_token,
        ];

        return $arrRtn;

    }
}