<?php

namespace Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 04/06/2019
 * Time: 11:01
 */
abstract class BaseModel extends EloquentModel
{

    /**
     * Save the model to the database.
     *
     * @param  array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        $this->preSave();
        $saved = parent::save();
        $this->postSave();

        return $saved;
    }

    /**
     * Action to be performed before the model is saved
     *
     * @return true
     * @throws \Exception
     */
    protected function preSave()
    {
        $this->validate();

        return true;
    }

    /**
     * Validates a model before saving it
     *
     * @return true
     * @throws \Exception
     */
    public function validate()
    {
        return true;
    }

    /**
     * Action to be performed before the model is saved
     *
     * @return true
     * @throws \Exception
     */
    protected function postSave()
    {
        return true;
    }

}