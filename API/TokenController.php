<?php
/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 12:41
 */

namespace API;

use API\Middleware\JWT;
use API\Middleware\RefreshToken;
use Helpers\Auth;
use Slim\Http\Request;
use Slim\Http\Response;
use Models\User;

class TokenController extends BaseAPI
{

    protected $basePath = '/access-tokens';

    /**
     * Initializes controller routes
     */
    public function index()
    {
        $this->app->post($this->basePath, [$this, 'login']);
        $this->app->post($this->basePath . '/refresh', [$this, 'refreshToken']);
        $this->app->delete($this->basePath, [
            $this,
            'logout',
        ])->add(new RefreshToken())->add(new JWT());

    }

    /**
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function refreshToken(Request $request, Response $response, $args)
    {
        try {
            $body = $request->getParsedBody();
            $token = Auth::refreshToken($body['refresh_token']);

            return $response->withJson($token)->withStatus(200);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function logout(Request $request, Response $response, $args)
    {
        try {
            $data = $request->getAttribute("token");
            $body = $request->getParsedBody();
            Auth::logout($data['id'], $body['refresh_token']);

            return $response->withStatus(204);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function login(Request $request, Response $response, $args)
    {
        try {
            $body = $request->getParsedBody();
            $data = Auth::login($body['email'], $body['password']);

            return $response->withJson($data)->withStatus(201);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

}