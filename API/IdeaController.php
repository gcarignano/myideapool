<?php
/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 12:41
 */

namespace API;

use API\Middleware\JWT;
use API\Middleware\RefreshToken;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use InvalidArgumentException;
use Models\ModelValidationException;
use Slim\Http\Request;
use Slim\Http\Response;
use Models\Idea;

class IdeaController extends BaseAPI
{

    protected $basePath = '/ideas';

    /**
     * Initializes controller routes
     */
    public function index()
    {
        $this->app->post($this->basePath, [
            $this,
            'create',
        ])->add(new RefreshToken())->add(new JWT());

        $this->app->delete($this->basePath . "/{id}", [
            $this,
            'deleteIdea',
        ])->add(new RefreshToken())->add(new JWT());

        $this->app->put($this->basePath . "/{id}", [
            $this,
            'updateIdea',
        ])->add(new RefreshToken())->add(new JWT());

        $this->app->get($this->basePath, [
            $this,
            'getIdeas',
        ])->add(new RefreshToken())->add(new JWT());

    }

    /**
     * Returns a list of ideas. 10 per page
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function getIdeas(Request $request, Response $response, $args)
    {
        try {
            $data = $request->getAttribute("token");
            $page = $request->getQueryParam('page', 1);
            $ideas = Idea::where([
                'user_id' => $data['id'],
            ])->orderBy('average_score', 'desc')->paginate(
                10,
                $columns = ['*'],
                $pageName = 'page',
                $page
            );

            $ideas_array = $ideas->toArray();
            $ideas_array = is_array($ideas_array['data'])
                ? $ideas_array['data']
                : [$ideas_array['data']];

            return $response->withJson($ideas_array)->withStatus(200);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * Updates an idea
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function updateIdea(Request $request, Response $response, $args)
    {
        try {
            $data = $request->getAttribute("token");
            $body = $request->getParsedBody();
            $idea = Idea::where([
                'id' => $args['id'],
                'user_id' => $data['id'],
            ])->firstOrFail();

            $fill = [
                'content' => null,
                'impact' => null,
                'ease' => null,
                'confidence' => null,
            ];

            $full = array_merge($fill, $body);
            $idea->fill($full);
            $idea->save();

            return $response->withJson($idea->toArray())->withStatus(200);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * Deletes an idea
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function deleteIdea(Request $request, Response $response, $args)
    {
        try {
            $data = $request->getAttribute("token");
            $idea = Idea::where([
                'id' => $args['id'],
                'user_id' => $data['id'],
            ])->firstOrFail();

            $idea->delete();

            return $response->withStatus(204);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * Creates a new idea
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function create(Request $request, Response $response, $args)
    {
        try {
            $body = $request->getParsedBody();
            $data = $request->getAttribute("token");

            $idea = new Idea($body);
            $idea->user_id = $data['id'];
            $idea->save();

            return $response->withJson($idea->toArray())->withStatus(201);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

}