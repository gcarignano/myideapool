<?php
/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 13:07
 */

namespace API\Middleware;

use function call_user_func;
use \Tuupola\Middleware\JwtAuthentication;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class JWT
{
    protected $JWT_middleware;

    /**
     * JWT constructor.
     */
    public function __construct()
    {
        $this->JWT_middleware = new JwtAuthentication([
            "header" => "x-access-token",
            "regexp" => "/(.*)/",
            "secure" => false,
            "secret" => JWT_SECRET,
        ]);
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param                                          $next
     *
     * @return mixed
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $next
    ) {
        return call_user_func($this->JWT_middleware, $request, $response,
            $next);
    }
}