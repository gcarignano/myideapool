<?php
/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 13:07
 */

namespace API\Middleware;

use Models\User;

class RefreshToken
{

    /**
     * @param $request
     * @param $response
     * @param $next
     *
     * @return mixed
     */
    public function __invoke($request, $response, $next)
    {
        try {
            $token = $request->getAttribute('token');
            if ($token) {
                $user = User::findOrFail($token['id']);
                if (!empty($user->refresh_token)) {
                    $response = $next($request, $response);

                    return $response;
                }
            }
        } catch (\Exception $e) {
            //failed cause the user was not found on the DB, Unauthorized
        }

        return $response->withStatus(401);
    }
}
