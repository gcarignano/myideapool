<?php

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 12:41
 */

namespace API;

use API\Middleware\JWT;
use API\Middleware\RefreshToken;
use Slim\Http\Request;
use Slim\Http\Response;
use Models\User;

class UserController extends BaseAPI
{
    protected $basePath = '/';

    /**
     * Initializes controller routes
     */
    public function index()
    {
        $this->app->post($this->basePath . "users", [$this, 'signup']);

        $this->app->get($this->basePath . "me", [
            $this,
            'me',
        ])->add(new RefreshToken())->add(new JWT());
    }

    /** Signs up a new user
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function signup(Request $request, Response $response, $args)
    {
        try {
            $body = $request->getParsedBody();
            $user = new User($body);
            $user->password = $body['password'];
            $user->refreshToken();
            $user->save();

            return $response->withJson($user->getLoginData())->withStatus(201);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }

    /**
     * Returns the current user's data
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param                     $args
     *
     * @return \Slim\Http\Response|static
     */
    public function me(Request $request, Response $response, $args)
    {
        try {
            $data = $request->getAttribute("token");
            $user = User::findOrFail($data['id']);

            return $response->withJson($user->toArray())->withStatus(200);
        } catch (\Exception $e) {
            return $this->handleException($e, $request, $response);
        }
    }
}