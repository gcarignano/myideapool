<?php

namespace API;

use Exception;
use Helpers\AuthException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use InvalidArgumentException;
use Models\ModelValidationException;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 08/06/2019
 * Time: 12:48
 */
abstract class BaseAPI
{
    protected $app;
    protected $basePath = '';

    /**
     * BaseAPI constructor.
     *
     * @param \Slim\App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->index();
    }

    /**
     * @param \Exception          $e
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     *
     * @return static
     */
    protected function handleException(
        Exception $e,
        Request $request,
        Response $response
    ) {
        if ($e instanceof ModelValidationException) {
            $error = [
                'detail' => $e->getMessage(),
                'status' => 400,
            ];
        } elseif ($e instanceof InvalidArgumentException || $e instanceof ModelNotFoundException) {
            $error = [
                'detail' => 'The provided information is not valid',
                'status' => 400,
            ];
        } elseif ($e instanceof TypeError) {
            $error = [
                'detail' => 'Please make sure you have provided all required fields',
                'status' => 400,
            ];
        } elseif ($e instanceof AuthException) {
            $error = [
                'detail' => $e->getMessage(),
                'status' => $e->getCode(),
            ];
        } elseif ($e instanceof Exception) {
            $error = [
                'detail' => 'Oops, something went terribly wrong, try again',
                'status' => 500,
            ];
        }

        return $response->withJson($error)->withStatus($error['status'])
            ->withHeader('Content-type',
                'application/problem+json'
            );
    }
}