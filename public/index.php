<?php

use API\IdeaController;
use API\TokenController;
use API\UserController;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/app-settings.php';
require __DIR__ . '/../src/eloquent-settings.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

$tokenCtrl = new TokenController($app);
$userCtrl = new UserController($app);
$ideaCtrl = new IdeaController($app);

// Run app
$app->run();
