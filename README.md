# Idea Pool API



## Install the Application

####Database

Database migration file located in '/DB/creation.sql'

Database configuration file is located in

        /src/eloquent-settings.php
        
For the database host you should use your own local IP address and set the port the same as the docker-compose.yml (defaults to 3303)

####Composer

Install dependencies via composer

        ./composer install

####Docker

Docker files are located the folder: 

        /docker

Web service will be bind to local port 8081 and MySQL will be bind to 3303 local port


From the /docker folder run the
        
        docker-compose up -d
        
You can reach the API through:

        http://localhost:8081/public/