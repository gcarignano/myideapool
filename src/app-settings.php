<?
define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
define('VENDOR_ROOT', BASEPATH . '/vendor');
define('JWT_SECRET', 'SomeVeryHardToGuessSecretKey');
define('JWT_ALGORITHM', 'HS256');
define('JWT_VALIDITY', '10 minutes');


spl_autoload_register('autoload');

function autoload($classname)
{
    $classname = str_replace('\\', '/', $classname);
    $filename = BASEPATH . '/' . $classname . '.php';
    if (file_exists($filename)) {
        require_once($filename);
    }
}
