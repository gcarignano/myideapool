CREATE DATABASE ideas_pool;
USE ideas_pool;

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `id`            INT          NOT NULL AUTO_INCREMENT,
  `email`         VARCHAR(100) NOT NULL,
  `name`          VARCHAR(100) NOT NULL,
  `password`      VARCHAR(100) NOT NULL,
  `avatar_url`    TEXT         NULL,
  `refresh_token` CHAR(255)    NOT NULL,
  `created_at`    INT(11)      NOT NULL,
  `updated_at`    INT(11)      NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `ideas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ideas` (
  `id`            INT          NOT NULL AUTO_INCREMENT,
  `user_id`       INT          NOT NULL,
  `content`       VARCHAR(255) NOT NULL,
  `impact`        SMALLINT(2)  NOT NULL,
  `ease`          SMALLINT(2)  NOT NULL,
  `confidence`    SMALLINT(2)  NOT NULL,
  `average_score` DOUBLE       NOT NULL,
  `created_at`    INT(11)      NOT NULL,
  `updated_at`    INT(11)      NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ideas_users_idx` (`user_id` ASC),
  CONSTRAINT `fk_ideas_users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB;