<?php

namespace Helpers;

use Exception;
use Models\User;
use function password_verify;

/**
 * Created by PhpStorm.
 * User: Gabo
 * Date: 04/06/2019
 * Time: 21:26
 */
class Auth
{

    /**
     * Checks if the user password is the same as the $comparePassword.
     *
     *
     * @param $userPassword    User's encrypted password
     * @param $comparePassword Decrypted compare password
     *
     * @return true
     * @throws \Exception
     */
    public static function passwordsMatch($userPassword, $comparePassword)
    {
        if (!password_verify($comparePassword, $userPassword)) {
            throw new AuthException('Password is incorrect', 401);
        }

        return true;
    }

    /**
     * Tries to login a user.
     * If valid it will refresh the refresh_token
     *
     * @param $email    User's email
     * @param $password User's decrypted password
     *
     * @return array
     * @throws \Exception
     */
    public static function login($email, $password)
    {
        try {
            $user = User::where(['email' => $email])->firstOrFail();
            self::passwordsMatch($user->password, $password);
        } catch (Exception $e) {
            throw new AuthException('Username or password incorrect', 401);
        }

        $user->refreshToken();
        $user->save();

        return $user->getLoginData();
    }

    /**
     * Deletes the refresh_token from the user
     *
     * @param $id
     * @param $refresh_token
     *
     * @return bool
     * @throws \Helpers\AuthException
     */
    public static function logout($id, $refresh_token)
    {
        try {
            $user = User::where([
                'id' => $id,
                'refresh_token' => $refresh_token,
            ])->firstOrFail();
        } catch (Exception $e) {
            throw new AuthException('Provided credentials are invalid', 401);
        }

        $user->refresh_token = null;
        $user->save();

        return true;
    }

    /**
     * Refreshes the JWT token
     *
     * @param $refresh_token
     *
     * @return mixed
     * @throws \Helpers\AuthException
     */
    public static function refreshToken($refresh_token)
    {
        try {
            $user = User::where([
                'refresh_token' => $refresh_token,
            ])->firstOrFail();
        } catch (Exception $e) {
            throw new AuthException('Provided credentials are invalid', 401);
        }

        return $user->getLoginData();
    }
}